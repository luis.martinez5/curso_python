# -*- coding: utf-8 -*-
#Martinez Salazar Luis Antonio
import sys
import csv
import xml.etree.ElementTree as tree
from datetime import datetime   

archivo = 'nmap.xml'

def Estado(archivo, state):
    list = [['IP\t', 'Estatus\t', 'Dominio']]
    with open(archivo, 'r') as archivo:
        aux = tree.fromstring(archivo.read())
        for host in aux.findall('host'):
            s = host.find('status').get('state')
            if(s == state):
                nombre = ''
                hostnames = host.find('hostnames')
                if(hostnames != None):
                    for hostname in hostnames.findall('hostname'):
                        if hostname.get('name'):
                            nombre = hostname.get('name') 
                addr = host.find('address').get('addr')
                list.append([addr, s, nombre])
        return list

def Puertos(archivo, portNumber):
    list = [['IP\t', 'Puerto\t', 'Estatus\t' 'Dominio']]
    with open(archivo, 'r') as archivo:
        aux = tree.fromstring(archivo.read())
        for host in aux.findall('host'):
            p = host.find('ports')
            if (p != None):
                for pid in p.findall('port'):
                    if (pid.get('portid') == portNumber and pid.find('state').get('state') == 'open'):
                        addr = host.find('address').get('addr')
                        nombre = ''
                        hostnames = host.find('hostnames')
                        if(hostnames != None):
                            for hostname in hostnames.findall('hostname'):
                                if hostname.get('name'):
                                    nombre = hostname.get('name')
                        list.append([addr, pid.get('portid'), pid.find('state').get('state'), nombre])          
        return list

def Server(archivo, nombre):
    list = [['IP\t', 'Servidor\t', 'Dominio\t']]
    nombre = ''
    with open(archivo, 'r') as archivo:
        aux = tree.fromstring(archivo.read())
        for host in aux.findall('host'):
            ports = host.find('ports')
            if (ports != None):
                for port in ports.findall('port'):
                    addr = host.find('address').get('addr')
                    for service in port.findall('service'):
                        if(service.get('product') and nombre in service.get('product').lower()):
                            hostnames = host.find('hostnames')
                            if(hostnames != None):
                                for hostname in hostnames.findall('hostname'):
                                    if hostname.get('name'):
                                        nombre = hostname.get('name')
                            list.append([addr, service.get('product'), nombre])
        return list


with open('resultados.cvs', 'wb') as arch:
    writer = csv.writer(arch)
    arch.write('Host prendidos')
    arch.write('\n')
    writer.writerows(Estado(archivo, 'up'))
    arch.write('\n')
    arch.write('Host apagados')
    arch.write('\n')
    writer.writerows(Estado(archivo, 'down'))
    arch.write('\n')
    arch.write('Con puerto 22 abierto')
    arch.write('\n')
    writer.writerows(Puertos(archivo, '22'))
    arch.write('\n')
    arch.write('Honeypots')
    writer.writerows(Server(archivo, 'dionaea'))
    arch.write('\n')


