#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT

eq1 = ['Juan Manuel','Ignacio','Valeria','Luis Antonio','Pedro Alejandro']
eq2 = ['Diana Guadalupe','Jorge Luis','Jesika','Jesus Enrique','Rafael Alejandro']
eq3 = ['Servando Miguel','Ricardo Omar','Laura Patricia','Isaias Abraham','Oscar']

#expresion funcional:
# 1) funcion lambda que sume las tres listas
# 2) filtre la lista resultante para obtener a los que tienen un solo nombre (filter)
# 3) convierta a mayusculas los nombres del resultado anterior (map)
# 4) obtener una cadena con los nombres resultantes, separando los nombres con coma (reduce)
#UNA SOLA EXPRESION


#lambda l1,l2,l3: l1.append(l2.append(l3)),eq1,eq2,eq3
#filter(lambda nombre: " " not in nombre, t_lista)
#map( lambda mayus: mayus.upper() , )
#reduce ( lambda c1,c2: "c1"+"c2" ,  )
"""

filter(lambda nombre: " " not in nombre, (lambda l1,l2,l3: l1.append(l2.append(l3)),eq1,eq2,eq3)
map( lambda mayus: mayus.upper() ,(filter ( lambda nombre: ' ' not in nombre, ( lambda l1,l2,l3: l1.append(l2.append(l3)),eq1,eq2,eq3 ) )) )
reduce ( lambda c1,c2: "c1"+"c2" , () )
"""

reduce ( lambda c1,c2: c1 "  " c2 , (map( lambda mayus: mayus.upper() ,filter ( lambda nombre: ' ' not in nombre, ( lambda l1,l2,l3: l1.append(l2.append(l3)),eq1,eq2,eq3 ) ) )) )
