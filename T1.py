 # -*- coding: utf-8 -*-
#Martinez Salazar Luis Antonio

def verificar(cadena):
	"""
	Esta funcion determina si una cadena es un palindromo
	Recibe		cadena
	Regresa		True si num es un palindromo y false si no es 
	"""
	v=cadena == cadena[::-1]
	return v

def pal(cadena):
	"""
	Funcion que obtiene el palíndromo más largo contenido en una cadena 
	Recibe		cadena donde la que se va a buscar el palíndromo 
	Regresa:	cadena con el palindromo más largo
	"""
	cad_palindromos = []
	for i,char in enumerate(cadena):
		indices = [ind for ind, a in enumerate(cadena) if a == char]
		indices = indices[indices.index(i):]
		for j in range(1, len(indices)):
			if(verificar(cadena[indices[0]: indices[j]+1])):
				cad_palindromos.append(cadena[indices[0]: indices[j]+1]) 
	if(len(cad_palindromos) > 0):
		cad_palindromos.sort(lambda x,y: cmp(len(x), len(y)))	
	
	return cad_palindromos[-1]


def factorial(num):
	factor=1
	for i in range(num, 1, -1):
		factor=factor * i	#teorema de Wilson
	return factor

def	num_primo(num):
	"""
	Esta funcion determina si un numero dado es primo
	Recibe		num : numero a evaluar
	Regresa		True si num es primo y false si no es primo  
	"""
	primo=((factorial(num-1) + 1) % num) == 0 
	return primo

def lista_primos(elementos, num=1):
	"""
	Esta funcion devuelve una lista de los primeros n numeros primos
	Recibe		elementos : numero de elementos de la lista
				num: numero a evaluar
	Regresa		lista de numeros primos
	"""
	
	if(elementos == 0):
		return []
	else:
		if(num_primo(num)):
			return [num] + lista_primos(elementos-1, num+1) 

	return lista_primos(elementos, num+1)
	
##Ejecucion

cad1 = 'Ana' 
cad2 = 'Reconocer'
cad3 = 'Anita lava la tina'


print(pal(cad1+cad2+cad3))

print(lista_primos(25))