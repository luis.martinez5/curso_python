#!/usr/bin/python
# -*- coding: utf-8 -*-
from random import choice, randint

def char_range(c1, c2):
    """Funcion para generar rangos de caracteres"""
    for c in xrange(ord(c1), ord(c2)+1):
        yield chr(c)

def password(longitud):
	password=[]
	i=0
	if longitud == 0:
		mensaje="Por favor ingrese un numero mas grande"
		return mensaje
	else:
		while i < longitud:
			password.append(choice(caracteres[randint(0,3)]))
			i+=1
		
		return reduce(lambda x,y: x+y,password)

minusculas=[]
mayusculas=[]
numeros=[]

for c in char_range('a', 'z'):
	minusculas.append(c)

for c in char_range('A', 'Z'):
	mayusculas.append(c)

for c in char_range('0', '9'):
	numeros.append(c)

caracteres=[['$','-','_','#','&'],minusculas,mayusculas,numeros]

print(password(10))

