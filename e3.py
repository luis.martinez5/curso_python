#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
from random import choice

calificacion_alumno = {} #diccionario vacio
calificaciones = (0,1,2,3,4,5,6,7,8,9,10)	#tupla
becarios = ['Juan Manual',
            'Ignacio',
            'Valeria',
            'Luis Antonio',
            'Pedro Alejandro',
            'Diana Guadalupe',
            'Jorge Luis',
            'Jesika',
            'Jesús Enrique',
            'Rafael Alejandro',
            'Servando Miguel',
            'Ricardo Omar',
            'Laura Patricia',
            'Isaías Abraham',
            'Oscar']

def asigna_calificaciones():
    for b in becarios:
        calificacion_alumno[b] = choice(calificaciones)

def imprime_calificaciones():
    for alumno in calificacion_alumno:
        print '%s tiene %s\n' % (alumno,calificacion_alumno[alumno])

def tuplas():
    aprobados=[]
    reprobados=[]

    for alumno,calif in calificacion_alumno.items():
        if calif >= 8:
            aprobados.append(alumno)
        else:
            reprobados.append(alumno)
    return tuple(aprobados),tuple(reprobados)

def promedio():
    count=0.0
    prom=0
    for b in calificacion_alumno.values():
        prom=prom+b
        count+=1

    prom=prom/count 

    return prom

def conjunto():
    return set([a for a in calificacion_alumno.values()])


asigna_calificaciones()
imprime_calificaciones()
print(tuplas())
print(promedio())
print(conjunto())






