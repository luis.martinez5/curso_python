# -*- coding: utf-8 -*-
#Martinez Salazar Luis Antonio

import sys
import datetime
import hashlib
import xml.etree.ElementTree as tree
from datetime import datetime   

archivo = 'nmap.xml'

def hash(archivo):
    hashmd5 = hashlib.md5()
    sha = hashlib.sha1()
    with open(archivo, "rb") as arc:
        for chunk in iter(lambda: arc.read(4096), b""):
            hashmd5.update(chunk)
            sha.update(chunk)
    return hashmd5.hexdigest(),sha.hexdigest()

def Http_S(archivo):
    aux = 0
    with open(archivo, 'r') as archivo:
        aux2 = tree.fromstring(archivo.read())
        for host in aux2.findall('host'):
            ports = host.find('ports')
            if (ports != None):
                for port in ports.findall('port'):
                    for service in port.findall('service'):
                        if(service.get('name') == 'http'):
                            aux += 1
        return str(aux)  

def Puertos(archivo, num_port):
    aux = 0
    with open(archivo, 'r') as archivo:
        aux2 = tree.fromstring(archivo.read())
        for host in aux2.findall('host'):
            p = host.find('ports')
            if (p != None):
                for pid in p.findall('port'):
                    if (pid.get('portid') == num_port and pid.find('state').get('state') == 'open'):
                        aux += 1
        return str(aux)

def Nombre_Dom(archivo):
    aux = 0
    with open(archivo, 'r') as archivo:
        aux2 = tree.fromstring(archivo.read())
        for host in aux2.findall('host'):
            hostnames = host.find('hostnames')
            if (hostnames != None):
                for hostname in hostnames.findall('hostname'):
                    if (hostname.get('name')):
                        aux += 1
        return str(aux)

def Host(archivo, state):
    aux = 0
    with open(archivo, 'r') as archivo:
        aux2 = tree.fromstring(archivo.read())
        for host in aux2.findall('host'):
            s = host.find('status').get('state')
            if(s == state):
                aux += 1
        return str(aux)      

def ServerType(archivo, serverName):
    aux = 0
    with open(archivo, 'r') as archivo:
        aux2 = tree.fromstring(archivo.read())
        for host in aux2.findall('host'):
            ports = host.find('ports')
            if (ports != None):
                for port in ports.findall('port'):
                    for service in port.findall('service'):
                        if(service.get('product') and serverName in service.get('product').lower()):
                            aux += 1
        return str(aux)    

def Otros(archivo, s1, s2, s3):
    aux = 0
    with open(archivo, 'r') as archivo:
        aux2 = tree.fromstring(archivo.read())
        for host in aux2.findall('host'):
            ports = host.find('ports')
            if (ports != None):
                for port in ports.findall('port'):
                    for service in port.findall('service'):
                        if(service.get('product') and (service.get('name') == 'http' or service.get('name') == 'https')):
                            if(s1 not in service.get('product').lower() and s2 not in service.get('product').lower() and s3 not in service.get('product').lower() ):
                                 aux += 1
        return str(aux)



   

hora = str(datetime.now())
md5,sha = hash(archivo)
encendido = Host(archivo, 'up')
apagado = Host(archivo, 'down')

np_22 = Puertos(archivo, '22')
np_53 = Puertos(archivo, '53')
np_80 = Puertos(archivo, '80')
np_443 = Puertos(archivo, '443')

domain = Nombre_Dom(archivo)
http = Http_S(archivo)

apache = ServerType(archivo, 'apache')
honeypot = ServerType(archivo, 'dionaea')
nginx = ServerType(archivo, 'nginx')

other = Otros(archivo, 'apache', 'nginx', 'dionaea')


print 'Hora:', hora
print 'MD5 de xml:', md5
print 'SHA1 de xml:', sha
print 'Host encendidos:', encendido
print 'Cantidad de hosts apagados:', apagado
print 'Hosts con puerto 22 abierto:', np_22
print 'Hosts con puerto 53 abierto:', np_53
print 'Hosts con puerto 80 abierto:', np_80
print 'Hosts con puerto 443 abierto:', np_443
print 'Hosts con nombre de dominio:', domain
print 'Servidores HTTP usados:', http
print 'Con Apache:', apache
print 'Con honeypots:', honeypot
print 'Con Nginx:', nginx
print 'Con otros servicios:', other

with open('resultados.txt', 'w+') as archivo_sal:
    archivo_sal.write('Hora: ' + hora)
    archivo_sal.write('\nMD5 de xml: ' + md5)
    archivo_sal.write('\nSHA1 de xml: ' + sha)
    archivo_sal.write('\nHosts encendidos: ' + encendido)
    archivo_sal.write('\nHosts apagados: ' + apagado)
    archivo_sal.write('\n'+ np_22 + ' tienen el puerto 22 abierto.')
    archivo_sal.write('\n'+ np_53 + ' tienen el puerto 53 abierto.')
    archivo_sal.write('\n'+ np_80 + ' tienen el puerto 80 abierto.')
    archivo_sal.write('\n'+ np_443 + ' tienen el puerto 443 abierto.')
    archivo_sal.write('\n'  + domain + ' con nombre de dominio.')
    archivo_sal.write('\n' + http + ' Servidores HTTP usados.')
    archivo_sal.write('\n' + apache + ' usan Apache.' )
    archivo_sal.write('\n'+ honeypot +' usan honeypots.' )
    archivo_sal.write('\n' + nginx + ' usan Nginx.' )
    archivo_sal.write('\n' + other +' usan otros servicios.')
